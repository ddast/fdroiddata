Categories:Money
License:MIT
Web Site:http://www.brentpanther.com
Source Code:https://github.com/hwki/SimpleBitcoinWidget
Issue Tracker:https://github.com/hwki/SimpleBitcoinWidget/issues
Bitcoin:15SHnY7HC5bTxzErHDPe7wHXj1HhtDKV7z

Auto Name:Simple Litecoin Widget
Summary:Show current Litecoin exchange prices
Description:
A clean and simple Litecoin widget to show the current price from different
exchanges.
.

Repo Type:git
Repo:https://github.com/hwki/SimpleBitcoinWidget.git

Build:1.0.7,8
    commit=ltc-1.0.7
    subdir=litecoin
    gradle=yes

Auto Update Mode:Version ltc-%v
Update Check Mode:Tags
Current Version:1.0.7
Current Version Code:8
